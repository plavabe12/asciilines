#!/usr/bin/env python3
# Copyright © 2019 Ebraheem AlAthari
# [This program is licensed under the "MIT License"]
# Please see the file LICENSE in the source
# distribution of this software for license terms.
import sys
import os

# Error message if not correctly read
def usage():
    print('asciilines: usage: asciilines [file location].tvg')
    pass

# Inserts characters into the canvas
# Return a modifed canvas
def insert_into_array(array_2d, char_to_insert):
    for i in range(0,len(char_to_insert)):
        char_to_add = char_to_insert[i][0]
        type_of_line = char_to_insert[i][3]
        row_pos = int(char_to_insert[i][1])
        col_pos = int(char_to_insert[i][2])
        if 0 <= int(char_to_insert[i][4]):
            for j in range(0,int(char_to_insert[i][4])):
                try:
                    if type_of_line == 'h':
                        array_2d[row_pos][col_pos] = char_to_add
                    elif type_of_line == 'v':
                        array_2d[row_pos][col_pos] = char_to_add
                except:
                    pass

                if type_of_line == 'h':
                    col_pos = col_pos + 1
                elif type_of_line == 'v':
                    row_pos = row_pos + 1
            pass
    return array_2d

# Takes the information read in and
# builds an empty canvas containing .
# returns the empty canvas and characters
# to insert
def build_array():
    rows, cols, char_to_insert = read_file()
    cols_array = []
    array_2d = []
    for i in range(0,rows):
        cols_array.append(['.'] * cols)
        array_2d.append(cols_array[i])
        pass
    return array_2d, char_to_insert

# Print the canvas and the characters inserted
def print_array(array_2d):
    for i in range(0,len(array_2d)):
        print_str = ''
        for j in range(0,len(array_2d[i])):
            print_str = print_str + array_2d[i][j]
            pass
        pass
        print(print_str)
    pass

# Reading the file in and producing
# information from the file
# Returning the information from the file
def read_file():
    file_object = open(sys.argv[1])
    first_line = file_object.readline().strip('\n')
    first_line = first_line.split(' ')
    rows, cols = int(first_line[0]),int(first_line[1])
    char_to_insert = []
    for line in file_object:
        line = line.strip('\n')
        line = line.split(' ')
        char_to_insert.append(line)
    return rows, cols, char_to_insert

def main():
    # Reads the file and creates a canvas and
    # stores the characters to add
    array_2d, char_to_insert = build_array()

    # Insert characters into canvas
    array_2d = insert_into_array(array_2d, char_to_insert)

    # Print Canvas
    print_array(array_2d)


if __name__ == '__main__':
    if len(sys.argv) != 2:
        usage()
        pass
    else:
        main()
        pass
