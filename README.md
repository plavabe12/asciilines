# asciilines
Copyright (c) 2019 Ebraheem AlAthari

This program takes a "Text Vector Graphics" file or "TVG" file and renders the file as ASCII on standard output.

## Run

You can run the program with `python3 asciilines.py` or `./asciilines.py`

    python3 asciilines.py test/test1.tvg

## Bugs

A bug in the program is that it panics when you pass in a non existent filename.

## License

This program is licensed under the "MIT License".  Please
see the file `LICENSE` in the source distribution of this
software for license terms.
